"""Defines how the diffusion data is stored."""
from pathlib import Path

import nibabel as nib
import numpy as np


class Acquisition:
    """Main parameters of diffusion MRI acquistion."""

    def __init__(self, bvals, bvecs):
        """Define diffusion MRI acquisition from bvals and bvecs arrays."""
        bvals = np.asarray(bvals)
        bvecs = np.asarray(bvecs)

        assert bvals.ndim == 1
        assert bvecs.ndim == 2
        self.bvals = bvals / 1000.0
        if bvecs.shape != (len(self), 3):
            bvecs = bvecs.T
        assert bvecs.shape == (len(self), 3)
        self.bvecs = bvecs

    def __len__(
        self,
    ):
        """Return number of volumes in acquisition."""
        return self.bvals.size

    @classmethod
    def read(cls, bvals_fn, bvecs_fn):
        """Read bvals and bvecs from disk."""
        return cls(np.genfromtxt(bvals_fn), np.genfromtxt(bvecs_fn))


class DiffusionMRI:
    """Represents diffusion MRI data."""

    def __init__(
        self, data: nib.Nifti1Image, mask: nib.Nifti1Image, acquisition: Acquisition
    ):
        """Set up diffusion MRI dataset."""
        assert data.ndim == 4
        assert mask.ndim == 3
        assert data.shape[-1] == len(acquisition)
        assert data.shape[:-1] == mask.shape
        self.nib_image = mask
        self.data = data.get_fdata()
        self.mask = mask.get_fdata() > 0
        self.acquisition = acquisition

    def masked_data(self, zslice=None):
        """
        Iterate over each voxel's data.

        Iteration can be limited to a single slice using `zslice`.
        """
        if zslice is None:
            return self.data[self.mask]
        else:
            return self.data[:, :, zslice][self.mask[:, :, zslice]]

    @classmethod
    def read(cls, data_fn, mask_fn, bvals_fn, bvecs_fn):
        """Read diffusion data from disk."""
        return cls(
            nib.load(data_fn),
            nib.load(mask_fn),
            Acquisition.read(bvals_fn, bvecs_fn),
        )

    @classmethod
    def read_dir(cls, directory):
        """
        Read diffusion data stored in a directory.

        Expects the same format as FSL bedpostX.
        """
        dir = Path(directory)
        return cls.read(
            dir / "data.nii.gz",
            dir / "nodif_brain_mask.nii.gz",
            dir / "bvals",
            dir / "bvecs",
        )


class DiffusionMRIMock:
    """
    Mock version of :class:`DiffusionMRI`.

    Can be created by directly supplying a predicted signal.
    :meth:`add_noise`: can be used to create multiple noisy iterations of the same base signal.
    """

    def __init__(self, data, acquisition):
        """Generate new mock :class:`DiffusionMRI` given some predicted data and acquisition."""
        data = np.asarray(data)
        if data.ndim == 1:
            data = np.asarray([data])
        self.data = data
        self.acquisition = acquisition

    def masked_data(self, zslice=None):
        """
        Return the provided data.

        It is called `masked_data` to match the same method in :class:`DiffusionMRI`.
        """
        assert zslice is None
        return self.data

    @classmethod
    def add_noise(cls, acquisition, base_signal, nrepeats, noise_level, rician=False):
        """
        Create multiple noisy version of the same `base_signal`.

        Each of the `nrepeats` signals will be fitted independently.
        Gaussian noise is added unless `rician` is set to True.
        """
        base_signal = np.asarray(base_signal)
        assert base_signal.ndim == 1
        if rician:
            data = [
                np.sqrt(
                    (base_signal + np.random.randn(base_signal.size) * noise_level) ** 2
                    + (np.random.randn(base_signal.size) * noise_level) ** 2
                )
                for _ in range(nrepeats)
            ]
        else:
            data = [
                (base_signal + np.random.randn(base_signal.size) * noise_level)
                for _ in range(nrepeats)
            ]
        return cls(data, acquisition)
