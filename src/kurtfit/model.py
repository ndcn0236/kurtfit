"""Defines DTI and DKI models to fit to the diffusion data."""

import numpy as np

from .data import Acquisition


class BaseDTI:
    """Model representing a diffusion tensor."""

    has_derivative = True

    def __init__(self, acquisition: Acquisition):
        """Run generic setup relevant for any DTI/DKI model."""
        self._acquisition = acquisition
        self.dti_mat_indices = np.array(
            [
                [1, 4, 5],
                [4, 2, 6],
                [5, 6, 3],
            ]
        )
        # 6xN array
        self.dti_A = np.asarray(
            [
                acquisition.bvals
                * np.sum(
                    (acquisition.bvecs @ (self.dti_mat_indices == index))
                    * acquisition.bvecs,
                    -1,
                )
                for index in range(1, 7)
            ]
        )

    @property
    def acquisition(
        self,
    ):
        """Acquisition parameters."""
        return self._acquisition

    def log_signal(self, params):
        """Compute log_signal given DTI paramaters."""
        return params @ self.A

    def log_jac_signal(self, params):
        """Compute both the log signal and its derivative."""
        extra_ndim = params.ndim - 1
        return params @ self.A, np.expand_dims(self.A.T, (0,) * extra_ndim)

    def signal(self, params):
        """Compute DWI signal given DTI parameters."""
        return np.exp(self.log_signal(params))

    def jac_signal(self, params):
        """Compute the signal and its derivative."""
        log_signal, log_jac = self.log_jac_signal(params)
        signal = np.exp(log_signal)
        jac = signal[..., np.newaxis] * log_jac
        return signal, jac

    def S0(self, params):
        """Return signal at b=0."""
        return np.exp(params[..., 0])

    def dti_mat(self, params):
        """Return diffusion tensor as matrix."""
        return params[..., self.dti_mat_indices]

    def eigen(self, params):
        """Return eigenvalues and eigenvectors of diffusion tensor."""
        if params.ndim == 1 or np.isfinite(params).all():
            return np.linalg.eigh(self.dti_mat(params))
        else:
            mask = (~np.isfinite(params)).any(-1)
            p2 = params.copy()
            p2[mask] = 0.0
            result = np.linalg.eigh(self.dti_mat(p2))
            result[0][mask] = np.nan
            result[1][mask] = np.nan
            return result

    def eigenvalues(self, params):
        """Return eigenvalues of diffusion tensor."""
        return self.eigen(params)[0]

    def eigenvectors(self, params):
        """Return eigenvectors of diffusion tensor."""
        return self.eigen(params)[1]

    def L1(self, params):
        """Return diffusivity along primary eigenvector."""
        return -self.eigenvalues(params)[..., 0]

    def V1(self, params):
        """Return primary eigenvector."""
        return self.eigenvectors(params)[..., 0]

    def L2(self, params):
        """Return diffusivity along secondary eigenvector."""
        return -self.eigenvalues(params)[..., 1]

    def V2(self, params):
        """Return secondary eigenvector."""
        return self.eigenvectors(params)[..., 1]

    def L3(self, params):
        """Return diffusivity along tertiary eigenvector."""
        return -self.eigenvalues(params)[..., 2]

    def V3(self, params):
        """Return tertiary eigenvector."""
        return self.eigenvectors(params)[..., 2]

    def MD(self, params):
        """Return mean diffusivity."""
        return -(params[..., 1] + params[..., 2] + params[..., 3]) / 3.0

    def FA(self, params):
        """Return fractional anisotropy."""
        ev = -self.eigenvalues(params)
        md = self.MD(params)
        return (
            np.sqrt(1.5)
            * np.sqrt(np.sum((ev - md[..., np.newaxis]) ** 2, -1))
            / np.sqrt(np.sum(ev**2, -1))
        )


class DTI(BaseDTI):
    """Diffusion tensor model without any kurtosis contribution."""

    def __init__(self, acquisition: Acquisition):
        """Set up fitting of diffusion tensor for a given acquistion."""
        super().__init__(acquisition)
        # (7, N) array
        self.A = np.concatenate(([np.ones(len(acquisition))], self.dti_A))


class DTIMeanKurt(BaseDTI):
    """Diffusion tensor model with a mean kurtosis parameter."""

    def __init__(self, acquisition: Acquisition):
        """Set up fitting of DTIMeanKurt model for a given acquisition."""
        super().__init__(acquisition)
        # (8, N) array
        self.A = np.concatenate(
            (
                [np.ones(len(acquisition))],
                self.dti_A,
                [acquisition.bvals**2 / 6.0],
            )
        )

    def kurt(self, params):
        """Mean kurtosis parameter."""
        return params[..., 7] / self.MD(params) ** 2


class DTIKurtDir(BaseDTI):
    """Diffusion tensor model with different kurtosis along each eigenvector."""

    has_derivative = False

    def log_signal(self, params):
        """Return the log(signal) for this model."""
        params = np.asarray(params)
        eigenvalues, eigenvectors = self.eigen(params)
        orient = (self.acquisition.bvecs @ eigenvectors) ** 2
        kurtosis = params[..., 7:] * eigenvalues**2 / 6.0
        return (
            params[..., 0, np.newaxis]
            + np.sum(orient * eigenvalues[..., np.newaxis, :], -1)
            * self.acquisition.bvals
            + np.sum(orient * kurtosis[..., np.newaxis, :], -1)
            * self.acquisition.bvals**2
        )

    def log_jac_signal(self, params):
        """Return the log(signal) and its derivative for this model."""
        raise NotImplementedError(
            "Analytic derivative calculation is not implement for DTIKurtDir."
        )

    def kurt1(self, params):
        """Kurtosis along primary eigenvector."""
        return params[..., 7]

    def kurt2(self, params):
        """Kurtosis along secondary eigenvector."""
        return params[..., 8]

    def kurt3(self, params):
        """Kurtosis along tertiary eigenvector."""
        return params[..., 9]

    def kurt(self, params):
        """Average of the directional kurtosis parameters."""
        return (
            self.kurt1(params) * self.L1(params) ** 2
            + self.kurt2(params) * self.L2(params) ** 2
            + self.kurt3(params) * self.L3(params) ** 2
        ) / self.MD(params) ** 2


class DKI(BaseDTI):
    """Full kurtosis model."""

    def __init__(self, acquisition: Acquisition):
        """Set up full kurtosis model fitting given acquisition."""
        super().__init__(acquisition)

        self.dki_mat_indices = np.zeros((3, 3, 3, 3), dtype=int)
        assigned_indices = []
        for indices in np.ndindex(self.dki_mat_indices.shape):
            base_indices = tuple(sorted(indices))
            if base_indices not in assigned_indices:
                assigned_indices.append(base_indices)
            self.dki_mat_indices[indices] = 7 + assigned_indices.index(base_indices)
        assert len(assigned_indices) == 15

        # 15xN array
        self.dki_A = np.asarray(
            [
                acquisition.bvals**2
                * np.einsum(
                    "ij, ik, il, im, jklm -> i",
                    acquisition.bvecs,
                    acquisition.bvecs,
                    acquisition.bvecs,
                    acquisition.bvecs,
                    self.dki_mat_indices == index,
                )
                / 6
                for index in range(7, 22)
            ]
        )

        self.A = np.concatenate(([np.ones(len(acquisition))], self.dti_A, self.dki_A))

    def dki_mat(self, params):
        """Return diffusion tensor as matrix."""
        return params[..., self.dki_mat_indices]

    def kurt_directional(self, params, vector):
        """Compute the kurtosis in a given direction."""
        dti = self.dti_mat(params)
        dti_mult = np.einsum("ij,ik,ijk->i", vector, vector, dti)
        dki = self.dki_mat(params)
        kurt_mult = np.einsum(
            "ij,ik,il,im,ijklm->i", vector, vector, vector, vector, dki
        )
        return kurt_mult / dti_mult**2

    def kurt1(self, params):
        """Compute kurtosis along primary eigenvector."""
        return self.kurt_directional(params, self.V1(params))

    def kurt2(self, params):
        """Compute kurtosis along secondary eigenvector."""
        return self.kurt_directional(params, self.V2(params))

    def kurt3(self, params):
        """Compute kurtosis along tertiary eigenvector."""
        return self.kurt_directional(params, self.V3(params))

    def kurt(self, params):
        """Average of the directional kurtosis parameters."""
        dki = self.dki_mat(params)
        total_w = (
            dki[..., 0, 0, 0, 0] + dki[..., 1, 1, 1, 1] + dki[..., 2, 2, 2, 2]
        ) / 3
        return total_w / self.MD(params) ** 2


class FreeWaterModel:
    """Model representing any `BaseDTI` model + free water."""

    def __init__(self, base: BaseDTI, diffusivity=3.0):
        """Set up free water model."""
        self.base = base
        self.diffusivity = diffusivity

    @property
    def has_derivative(
        self,
    ):
        """True if model has an analytic derivative."""
        return self.base.has_derivative

    @property
    def acquisition(
        self,
    ):
        """Data acquisition parameters."""
        return self.base.acquisition

    def jac_signal(self, params):
        """Return the signal and its derivative."""
        base_signal, base_jac = self.base.jac_signal(params[..., :-1])
        free_signal = np.exp(
            params[..., -1, np.newaxis] - self.diffusivity * self.acquisition.bvals
        )
        total_jac = np.concatenate((base_jac, free_signal[..., np.newaxis]), -1)
        return base_signal + free_signal, total_jac

    def log_jac_signal(self, params):
        """Return the log signal and its derivative."""
        signal, jac = self.jac_signal(params)
        return np.log(signal), jac / signal[..., np.newaxis]

    def signal(self, params):
        """Return the signal."""
        return np.exp(
            params[..., -1] - self.diffusivity * self.acquisition.bvals
        ) + self.base.signal(params[..., :-1])

    def log_signal(self, params):
        """Return the log signal."""
        return np.log(self.signal(params))
