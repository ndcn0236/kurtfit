"""Fit the model to the data."""
import argparse

import nibabel as nib
import numpy as np
from scipy import optimize, special

from .data import DiffusionMRI
from .model import DKI, DTI, BaseDTI, DTIKurtDir, DTIMeanKurt, FreeWaterModel


def set_min_log_signal(log_signal, log_S0):  # noqa: N803
    """
    Set minimum signal to 0.01 of S0 signal.

    This overwrites the signal in place.
    """
    _, all_log_S0 = np.broadcast_arrays(log_signal, log_S0[:, np.newaxis])  # noqa: N806
    replace = log_signal < np.log(0.01) + all_log_S0
    log_signal[replace] = all_log_S0[replace] + np.log(0.01)


def fit_linear(data: DiffusionMRI, model: BaseDTI, zslice=None):
    """Linear fit of the model to the log signal."""
    assert hasattr(model, "A")
    signal = np.log(np.maximum(data.masked_data(zslice=zslice), 1e-6))
    init = np.linalg.lstsq(model.A.T, signal.T, rcond=None)[0].T
    set_min_log_signal(signal, init[..., 0])
    return np.linalg.lstsq(model.A.T, signal.T, rcond=None)[0].T


def cost_func_sse(full_model, log=False):
    """Generate sum of squared error cost function."""
    if full_model.has_derivative:
        compare_with = full_model.log_jac_signal if log else full_model.jac_signal

        def cost_func(p, observed):
            signal, jac = compare_with(p)
            offset = signal - observed
            res = np.sum(offset**2), 2 * (offset @ jac)
            return res

    else:
        compare_with = full_model.log_signal if log else full_model.signal

        def cost_func(p, observed):
            return np.sum((compare_with(p) - observed) ** 2)

    return cost_func


def cost_func_rice(full_model, log=False):
    """Generate Rician cost function."""
    if full_model.has_derivative:
        compare_with = full_model.log_jac_signal if log else full_model.jac_signal

        def cost_func(p, observed):
            ivar = np.exp(-p[-1])
            predicted, jac = compare_with(p[:-1])

            zero_bessel = special.i0e(predicted * observed * ivar)
            bessel_ratio = special.i1e(predicted * observed * ivar) / zero_bessel

            full_prob = np.sum(
                np.log(observed)
                + np.log(ivar)
                - ivar * (observed**2 + predicted**2) / 2
                + predicted * observed * ivar
                + np.log(zero_bessel)
            )

            dprob_dtheta = (ivar * (-predicted + observed * bessel_ratio)) @ jac

            full_dprob = np.append(
                dprob_dtheta,
                -ivar
                * np.sum(
                    1 / ivar
                    - (observed**2 + predicted**2) / 2
                    + observed * predicted * bessel_ratio
                ),
            )

            return -full_prob, -full_dprob

    else:
        compare_with = full_model.log_signal if log else full_model.signal

        def cost_func(p, observed):
            ivar = np.exp(-p[-1])
            predicted = compare_with(p[:-1])
            return -np.sum(
                np.log(observed)
                + np.log(ivar)
                - ivar * (observed**2 + predicted**2) / 2
                + predicted * observed * ivar
                + np.log(special.ioe(observed * predicted * ivar))
            )

    return cost_func


def fit_non_linear(  # noqa: C901
    data: DiffusionMRI,
    model: BaseDTI,
    init,
    log=False,
    zslice=None,
    free_water=False,
    rician=False,
):
    """Non-linear fit of the model to the signal."""
    signal = data.masked_data(zslice=zslice)
    if log:
        signal = np.log(signal)
        set_min_log_signal(signal, init[..., 0])

    if free_water:
        # initialise parameters as described by:
        # 1. Hoy, A.R. et al. (2014) ‘Optimization of a free water elimination two-compartment model for diffusion tensor imaging’, NeuroImage, 103, pp. 323–333. doi:10.1016/j.neuroimage.2014.09.053.  # noqa: E501
        # 2. Henriques, R.N. et al. (2017) ‘[Re] Optimization of a free water elimination two-compartment model for diffusion tensor imaging’. bioRxiv, p. 108795. doi:10.1101/108795.  # noqa: E501
        low_mean_diff = model.MD(init) <= 1.5
        init = np.concatenate([init, init[:, 0, np.newaxis]], axis=1)
        init[low_mean_diff, 0] += np.log(2 / 3)
        init[low_mean_diff, -1] += np.log(1 / 3)
        init[~low_mean_diff, 0] += np.log(0.05)
        init[~low_mean_diff, -1] += np.log(0.95)

    if rician:
        # initialise noise variance assuming SNR of 10
        init = np.concatenate([init, 2 * init[:, 0, np.newaxis] - np.log(10)], axis=1)

    params = np.zeros_like(init)

    full_model = FreeWaterModel(model) if free_water else model

    get_cost_func = cost_func_rice if rician else cost_func_sse
    cost_func = get_cost_func(full_model, log=log)

    mse = np.zeros(params.shape[:-1])

    for index in range(signal.shape[0]):
        try:
            params[index] = optimize.minimize(
                cost_func, init[index], (signal[index],), jac=full_model.has_derivative
            ).x
            use_params = params[index][:-1] if rician else params[index]
            mse[index] = np.mean((full_model.signal(use_params) - signal[index]) ** 2)
        except RuntimeError:
            params[index] = np.nan
    return params, mse


def cli():  # noqa: C901
    """Run the `kurtfit` command line interface."""
    parser = argparse.ArgumentParser(
        description="Equivalent of dtifit implemented in python."
    )
    parser.add_argument("-o", "--out", help="output basename.", required=True)
    parser.add_argument("-k", "--data", help="dti data file")
    parser.add_argument("-m", "--mask", help="binary mask file")
    parser.add_argument("-r", "--bvecs", help="b vectors file")
    parser.add_argument("-b", "--bvals", help="b values file")
    parser.add_argument("-d", "--dir", help="directory with diffusion data")
    parser.add_argument(
        "--model", choices=["dti", "dti-kurt", "dti-kurtdir", "dki"], required=True
    )
    parser.add_argument(
        "--rician",
        action="store_true",
        help="assumes a Rician rather than Gaussian noise model",
    )
    parser.add_argument(
        "-f",
        "--free-water",
        action="store_true",
        help="add a free water component to the fit with a diffusivity of 3 um^2/ms",
    )
    parser.add_argument(
        "-z", "--zslice", type=int, help="run kurtfit in a single slice"
    )
    parser.add_argument(
        "-l",
        "--log",
        action="store_true",
        help="fit the log data rather than the actual data.",
    )
    args = parser.parse_args()
    if args.dir is not None:
        data = DiffusionMRI.read_dir(args.dir)
    else:
        data = DiffusionMRI.read(args.data, args.mask, args.bvals, args.bvecs)

    linear_models = {
        "dti": DTI,
        "dti-kurt": DTIMeanKurt,
        "dki": DKI,
    }

    if args.model in linear_models:
        model = linear_models[args.model](data.acquisition)
        init_params = fit_linear(data, model, args.zslice)
        if args.log and not args.free_water:
            params = init_params
            if args.zslice is None:
                mse = np.mean((data.data[data.mask] - model.signal(params)) ** 2, -1)
            else:
                mse = np.mean(
                    (
                        data.data[:, :, args.zslice][data.mask[:, :, args.zslice]]
                        - model.signal(params)
                    )
                    ** 2,
                    -1,
                )
        else:
            params, mse = fit_non_linear(
                data,
                model,
                init_params,
                log=args.log,
                zslice=args.zslice,
                free_water=args.free_water,
                rician=args.rician,
            )
    elif args.model == "dti-kurtdir":
        init_model = DTIMeanKurt(data.acquisition)
        part_init_params = fit_linear(data, init_model, args.zslice)
        init_kurt = (
            part_init_params[:, -1, np.newaxis]
            / init_model.eigenvalues(part_init_params) ** 2
        )
        init_params = np.concatenate((part_init_params[..., :-1], init_kurt), -1)
        model = DTIKurtDir(data.acquisition)
        params, mse = fit_non_linear(
            data,
            model,
            init_params,
            log=args.log,
            zslice=args.zslice,
            free_water=args.free_water,
            rician=args.rician,
        )
    else:
        raise ValueError("Unexpected --model flag")

    store_mask = data.mask
    if args.zslice is not None:
        store_mask = np.array(store_mask)
        store_mask[:, :, : args.zslice] = False
        store_mask[:, :, args.zslice + 1 :] = False

    fn = f"{args.out}_rmse.nii.gz"
    tostore = np.zeros(data.mask.shape)
    tostore[store_mask] = np.sqrt(mse)
    nib.Nifti1Image(tostore, data.nib_image.affine).to_filename(fn)

    if args.rician:
        fn = f"{args.out}_std.nii.gz"
        tostore = np.zeros(data.mask.shape)
        tostore[store_mask] = np.sqrt(np.exp(params[..., -1]))
        nib.Nifti1Image(tostore, data.nib_image.affine).to_filename(fn)

        params = params[..., :-1]

    for extension in [
        "S0",
        "MD",
        "FA",
        "V1",
        "V2",
        "V3",
        "L1",
        "L2",
        "L3",
        "kurt",
        "kurt1",
        "kurt2",
        "kurt3",
    ]:
        if hasattr(model, extension):
            fn = f"{args.out}_{extension}.nii.gz"
            tostore_1d = getattr(model, extension)(params)
            tostore = np.zeros(data.mask.shape + tostore_1d.shape[1:])
            tostore[store_mask] = tostore_1d
            nib.Nifti1Image(tostore, data.nib_image.affine).to_filename(fn)

    if args.free_water:
        full_S0 = np.exp(params[:, 0]) + np.exp(params[:, -1])  # noqa: N806

        fn = f"{args.out}_FW.nii.gz"
        tostore_1d = np.exp(params[:, -1]) / full_S0
        tostore = np.zeros(data.mask.shape + tostore_1d.shape[1:])
        tostore[store_mask] = tostore_1d
        nib.Nifti1Image(tostore, data.nib_image.affine).to_filename(fn)

        fn = f"{args.out}_S0.nii.gz"
        tostore_1d = full_S0
        tostore = np.zeros(data.mask.shape + tostore_1d.shape[1:])
        tostore[store_mask] = tostore_1d
        nib.Nifti1Image(tostore, data.nib_image.affine).to_filename(fn)
