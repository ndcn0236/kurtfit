[![PyPI - Downloads](https://img.shields.io/pypi/dm/kurtfit)](https://pypi.org/project/kurtfit/)
[![Documentation](https://img.shields.io/badge/Documentation-kurtfit-blue)](https://open.win.ox.ac.uk/pages/ndcn0236/kurtfit)
[![Pipeline status](https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit/badges/main/pipeline.svg)](https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit/-/pipelines/latest)
[![Coverage report](https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit/badges/main/coverage.svg)](https://open.win.ox.ac.uk/pages/ndcn0236/kurtfit/htmlcov)

Python version of FSL dtifit, which is mostly equivalent to dtifit and used for testing and comparing output parameters between parameters.

It can fit the diffusion tensor, the kurtosis model, and the approximations to the kurtosis model used in dtifit.
Each of these can be combined with a free water component.
The model can be fitted to the signal or to the log-signal.
It is *much* slower than dtifit!

# Installation
```shell
pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit.git
```

Any bug reports and feature requests are very welcome (see [issue tracker](https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit/-/issues)).

# Running kurtfit
After installation, `kurtfit` will be available from the command line.
You can get usage help by running `kurtfit --help`:
```
usage: kurtfit [-h] -o OUT [-k DATA] [-m MASK] [-r BVECS] [-b BVALS] [-d DIR] [-f] [-z ZSLICE] [-l] --model
               {dti,dti-kurt,dti-kurtdir,dki}

Equivalent of dtifit implemented in python.

options:
  -h, --help            show this help message and exit
  -o OUT, --out OUT     output basename.
  -k DATA, --data DATA  dti data file
  -m MASK, --mask MASK  binary mask file
  -r BVECS, --bvecs BVECS
                        b vectors file
  -b BVALS, --bvals BVALS
                        b values file
  -d DIR, --dir DIR     directory with diffusion data
  -f, --free-water      add a free water component to the fit with a diffusivity of 3 um^2/ms
  -z ZSLICE, --zslice ZSLICE
                        run kurtfit in a single slice
  -l, --log             fit the log data rather than the actual data.
  --model {dti,dti-kurt,dti-kurtdir,dki}
```

# Developer documentation
## Setting up local test environment
First clone the repository:
```shell
git clone https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit.git
```

Then, we install the package in an interactive manner:
```shell
cd kurtfit
pip install -e .
```

Development tools can be installed using:
```
pip install -r requirements_dev.txt
pre-commit install  # installs pre-commit hooks to keep the code clean
```


## Running tests
Tests are run using the [pytest](https://docs.pytest.org) framework. They can be run from the project root as:
```shell
pytest src/tests
```

## Compiling documentation
The documentation is build using [sphinx](https://www.sphinx-doc.org/en/master/). After installation (`pip install sphinx`) run:
```shell
cd doc
sphinx-build source build
open build/index.html
```

## Contributing
[Merge requests](https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit/-/merge_requests) with any bug fixes or documentation updates are always welcome.

For new features, please raise an [issue](https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit/-/issues) to allow for discussion before you spend the time implementing them.

## Releasing new versions
- Run `bump2version` (install using `pip install bump2version`)
- Push to gitlab
- Manually activate the "to_pypi" task in the [pipeline](https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit/-/pipelines/latest) to upload to pypi
