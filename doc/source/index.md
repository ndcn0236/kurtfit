# Welcome to kurtfit's documentation!

kurtfit

Install using

```
pip install git+https://git.fmrib.ox.ac.uk/ndcn0236/kurtfit.git
```

```{toctree}
:maxdepth: 3

kurtfit
```
